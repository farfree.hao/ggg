# pyJTest

## Getting started

A framework that run testcases under cases/ for testing DUT interactive by telnet/ssh.

## Architecture
---
![](doc/pyJTest.png)



## Testing Machine / Installation
---
Suggest use Ubuntu 20.04 or 22.04 LTS

Install following python package
```
1. sudo apt install python3-pip
2. sudo pip install ping3
3. sudo pip install scapy

* 因為scapy 需要使用root priviledge, 所以相關lib 需要用root 安裝, 才可以找的到
  官方建議用venv 的方式. 不過我不會用, 有興趣可以參考
* It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
```
&nbsp;

## Before Start
---
&nbsp;

![](doc/pyJTestVM.png)

* 在執行之前唯一需要配置的只有 environment.py,  針對測試環境填好值即可.
* 目前完整測試需要至少三條link 接到DUT，請參考上圖．
* 如果網卡不夠, 無法執行封包測試, 請設定PKTTEST = False.
* 底下是所有ENV define.

``` python
class ENVKEY(Enum):
    NAME    = "name"        # The test excute name
    DEV     = "dev"         # device model, reference to DEVMODEL enum
    IPADDR  = "ipaddr"      # device mgmt ip address
    PORT    = "port"        # telent port
    USERNAME = "username"   # username to login CLI
    PASSWORD = "password"   # password to login CLI
    SSHKEY   = "sshkey"     # ssh private key file. But not support ssh so far.
    PROMPT  = "prompt"      # prompt string of CLI
    MGMTLP  = "mgmtlp"      # mgmt logic port of DUT. some testcase may needs to skip mgmt lp
    SCAPYTX  = "scapyTx"    # netdevice name for scapy tx port of VM.
    SCAPYRX  = "scapyRx"    # netdevice name for scapy rx port of VM.
    DUTTX  = "DUTTx"        # Tx logic port of DUT for packet test.
    DUTRX  = "DUTRx"        # Rx logic port of DUT for packet test.
    #suggest don't use the same port as mgmt for testing tx / rx port
    # SCAPYTX -> DUTRX -> DUTTX -> SCAPYRX
    TESTCATEGORY  = "TestCategory" # define test categfory for test from main. If empty means test all category and cases
    QUICKTEST = "quickTest"        # quick test bit. In case which can use this bit for quick test (bypass some step) if owner want.
    PKTTEST = "pktTest"            # pkt test bit. In some VM may not support run pkt test. Can disale pkt test by this bit without change case.
```
``` python
#example
ENV = {
    ENVKEY.NAME:        "JTest",
    ENVKEY.DEV:         DEVMODEL.GS728TPV3,
    ENVKEY.IPADDR:      "192.168.1.239",
    ENVKEY.USERNAME:    "",
    ENVKEY.PASSWORD:    "",
    ENVKEY.SSHKEY:      "pkeyfile",
    ENVKEY.PROMPT:      "GS728TPV3",
    ENVKEY.PORT:        60000,
    ENVKEY.MGMTLP:      "gi7",
    ENVKEY.DUTTX:       "gi5",
    ENVKEY.DUTRX:       "gi23",
    ENVKEY.SCAPYTX:     "ens36",
    ENVKEY.SCAPYRX:     "ens37",
    ENVKEY.TESTCATEGORY: ["vlan"],
    ENVKEY.QUICKTEST: True,
    ENVKEY.PKTTEST: False,
}
```

## pylint
---
Ref: https://wiki.realtek.com/display/SS/SD6+Python+Style+Guide
目錄底下有一個預設的.pylintrc,  從sd6 pylint 範本copy 而來, 經過J 精心調整後, 符合J 開發習慣.
建議在vscode 開啟pylint, 讓test case 都符合規範.

How to enable pylint in vscode.
https://code.visualstudio.com/docs/python/linting


## Getting Start
---
### Run Single Case
```
python3 runSingleCase.py -t cases/$category/$case

EX:
python3 runSingleCase.py -t cases/poc/01_poc_ok.py
```

### Run Whole Case
```
python3 main.py
```
* main.py會參考 environment.py 裡面的 TESTCATEGORY 參數.  如果List 為空,  則會執行cases 底下所有的test cases.
* 如果只想要執行特定的categories, 可直接在List 當中填入category 目錄名稱.
EX: ["vlan", "ucal_mac"]


## Add your cases
身為一個R豬, 程序猿, 螞膿,  It only needs
**copy exist, modify it. work!!**

* cases/poc/04_poc_template.py
    >基本的test case 驗證, 包含config, 驗證show commandm, runnning config 和 diag shell.


* cases/poc/05_poc_template_pkt.py
    > 包含封包測試的 temlate

&nbsp;
&nbsp;

### Test case concept
---
#### 1. About your case

test case 必須繼承 TestCase 這個class, 一開始有三個參數是關於這個test case 描述, 請將case 資訊描述在此

```python
class Testcase(TestCase):
    CASENAME    = "The test case name"
    SUBJECT     = "The subject for this case"
    DESCRIPTION = "More detail description for this case"

    def __init__(self, device, logger):
        TestCase.__init__(self, device, logger)

        # Start Your command and steps here
```

&nbsp;
&nbsp;

#### 2. Test case concept - Command

先看範例. 不懂在看後面
``` python
# 1. Create Vlan without verify
Command(f"vlan {vid}", CLINode.CONFIG, None)

# 2. Change vlan name
Command(f"name {name}", CLINode.CONFIG_SUB, None)

# 3. Verify show vlan
Command(f"show vlan {vid}", CLINode.PRIV, StringResultChk(verifyFmt, nospace=True))

# 4. Verify SDK vlan table
Command(f"vlan get vlan-table vid {vid}", CLINode.SDKDIAG, StringResultChk("MemberPorts", nospace=True))

# 5. play packet
Command(packets.pktEtherVlan(vid=vid), CLINode.PKT, packets.PktResultchk(pktFilter))
```

***[累了嗎? 聽首歌吧](https://www.youtube.com/watch?v=mhnn9TZTE3w)***

> 在test case 裡面, 每一個step 等於是一個命令, 由Command 這個Class 所提供.  包含三個參數 (cmd, node, verify)
> * **cmd** - 等於 action , 也就是你要執行命令, 可以是Turnkey CLI / Diag command / Scapy Packet
>
> * **node** -  Wehere to excute action. 代表action 需要在哪裡執行. 目前支援底下五種node.
其中只有CLINode.CONFIG_SUB 需要writer 自行控制進入, tester 會使用當下的狀態來執行command. EX: 想要設定interface 底下的command. 則前面需要自行加上 interface gi1 這樣的command. 其餘的Node 都不需要額外先做任何事情, 會自動切換過去.
>
>   | Node                | Descriotion   |
>   | --------------------|:------------- |
>   | CLINode.PRIV        | CLI priviledge node      |
>   | CLINode.CONFIG      | CLI config node          |
>   | CLINode.CONFIG_SUB  | CLI sub config node      |
>   | CLINode.SDKDIAG     | SDK diag node            |
>   | CLINode.PKT         | Play scapy packet        |
>
> * **verify (option)** -  如果在這個step 需要有verify 的動作, 則是利用這個callback function 來執行確認. 不需要verify 則填入None 即可. 目前有兩個common verify callback, 一個是StringResultChk, 一個是 PktResultchk.  Test case pass or failed 就是由 verify callback return True or False 來決定, 只要任一Command 的verify callback return False, 就代表test case failed.


##### Custom Verify Callback Function
如果common callback 無法滿足需求, 則需要自己寫verify callback. Verify callback 需要繼承VerifyCB 這個抽象類別, 並且implement resultChk 這個abstractmethod. VerifyCB 只需要傳入一個cookie, 這可以是任意內容的 一個參數, 目的在之後用於輔助驗證result 是否正確. Tester 會將原本的cmd 跟 response 以及cookie 傳入 resultChk 這個API

```python
class VerifyCB(ABC):
    def __init__(self, cookie):
        self.cookie = cookie

    @abstractmethod
    #in packet check the parameter is self, pkt, filter, resp
    def resultChk(self, cmd, cookie, resp):
        return NotImplemented

```

#### 3. Test case concept - Command List

一個test case 就是由多個commands 所組成一連串的動作. 在TestCase 裡面一共有三個Command List, 執行順序為
preCmds ==> tCmds ==> postCmds, 各自有不同的目的.

##### Pre Commands (option)
> 在這個test case 一開始想要預先清除或是配置的一些動作. 與主要test 項目無關. 通常不需要搭配verify check.
> EX: create VLAN, enable port etc..
>
> **建議每個test case 都要能獨立運作, 不要依賴不同 test case dependency**


##### Test Commands
> Test case 主要的執行內容. 通常會搭配verify check

##### Post Commands (option)
> 在執行完主要測試之後, 想要刪除或還原的動作. 否則很容易造成後續的test case 因為非預期的config 而測試失敗.


## Supported Device
---
定義在 deviceModels.py, 可自行擴充

```python
# 1. add model enum
class DEVMODEL(Enum):
    GS728TPV3 = "GS728TPV3"
    GS728TPPV2  = "GS728TPPv2"
    MS510TUP  = "MS510TUP"

# 2. define capability. If common INTF list not support, define new one.
GS728TPV3_CAPA = {
    DeviceCapa.MAX_VLAN: 256,
    DeviceCapa.MAX_ACL:  100,
    DeviceCapa.INTFS: INTF24_4
}

# 3. add to dictionary
Capability = {
    DEVMODEL.GS728TPV3: GS728TPV3_CAPA,
    DEVMODEL.GS728TPPV2: GS728TPV3_CAPA, #share same capa, not bug
    DEVMODEL.MS510TUP: NGJ_CAPA
    }
```

## Collaborate with your team

Repo in here

git clone "ssh://yourname@cn2sd6.sdlc.rd.realtek.com:29418/MS/Turnkey/test/autotester"


## Test and Deploy
N/A

## Name
JTest

## Description
JTest

## Support
No Support

## Roadmap
No Roadmap

## Contributing
No Contributing

## Authors and acknowledgment
Thanks Crab

## License
Show me the money

## Project status
Corrupting
